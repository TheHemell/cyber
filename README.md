# Сборка Gulp for MODX Revo (fenom, stylus)

## Установка
Уже должен быть установлен Node.js

### Глобальная установка Gulp и Bower
Если ранее установка была произведена, то переходим к инициализации
```
npm install -g gulp-cli bower
```

### Инициализация NPM и Bower
```
npm init
```
В сборке по умолчанию: 
`jquery,normalize.css,bootstrap,open-sans-fontface,fancybox,swiper,svg4everybody`. Для редактирования списка смотрите файл `bower.json`
```
bower init
```

Список пакетов bower можно посмотреть на сайте https://bower.io/search/