'use strict';

// Подключение плагинов через переменные
var gulp = require('gulp'), // Gulp
    concat = require('gulp-concat'), // Объединение файлов
    plumber = require('gulp-plumber'), // Обработка ошибок
    rename = require('gulp-rename'), // Переименование файлов
    stylus = require('gulp-stylus'), // Stylus
    sourcemaps = require('gulp-sourcemaps'), // Карта css
    uglify = require('gulp-uglify'), // Минификация JS-файлов
    svgSprite = require('gulp-svg-sprite'), // Склеивание svg в один
    cheerio = require('gulp-cheerio'),
    nib = require('nib'),
    rupture = require('rupture'),
    postcss = require('gulp-postcss'),
    cssnano = require('gulp-cssnano'), // плагин postcss для сжатия
    autoprefixer = require('autoprefixer'); // плагин postcss для сжатия для Добавление вендорных префиксов

// Задание путей к используемым файлам и папкам
var paths = {
        watch: {
            styl: './app/css/common.styl',
            js: './app/js/common.js',
            svg: './app/materials/svg/*.svg',
            img: [
                './app/materials/images/**/*',
                './app/materials/images/*'
            ]
        },
        dist: {
            css: './dist/assets/css',
            fonts: './dist/fonts',
            js: './dist/assets/js',
            img: './dist/assets/images',
            svg: './dist/assets/images/svg'
        },
        app: {
            common: {
                styl: './app/css/common.styl',
                js: './app/js/common.js',
                css: [
                    './app/materials/fonts/**/*.css',
                    './app/materials/fonts/*.css'
                ],
                fonts: [
                    './app/materials/fonts/**/*.{ttf,woff,woff2,svg,eot}',
                    './app/materials/fonts/*.{ttf,woff,woff2,svg,eot}'
                ],
                img: [
                    './app/materials/images/**/*.{jpg,jpeg,png}',
                    './app/materials/images/*.{jpg,jpeg,png}'
                ],
                svg: './app/materials/svg/*.svg'
            },
            vendor: {
                fonts: [
                    './bower_components/montserrat-fonts-sources/fonts/webfonts/**/*.{ttf,woff,woff2,svg,eot}'
                ],
                css: [
                    './bower_components/montserrat-fonts-sources/fonts/webfonts/Montserrat.css',
                    './bower_components/normalize.css/normalize.css',
                ],
                js: [
                    './bower_components/jquery/dist/jquery.min.js',
                    './bower_components/svg4everybody/dist/svg4everybody.min.js',
                ]
            }
        }
    };

// Наблюдаем за изменениями
function server() {
    /* browserSync.init({
        server: paths.dist.php
    });*/
    gulp.watch(paths.watch.img).on('all', function ($action,$file) {
        img('./'+$file.replace(/\\/g,"/"));
    });
    gulp.watch(paths.watch.styl, gulp.series('cssCommon'));
    gulp.watch(paths.watch.js, gulp.series('jsCommon'));
    gulp.watch(paths.watch.svg, gulp.series('spritesSvg'));
}

// Для объединения шрифтов
function fonts() {
    var fonts = paths.app.vendor.fonts.concat(paths.app.common.fonts);
    return gulp.src(fonts)
        .pipe(gulp.dest(paths.dist.fonts));
}

// Для преобразования Stylus-файлов в CSS
function cssCommon() {
    return gulp.src(paths.app.common.styl)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(stylus({use:[nib(),rupture()]}))
        .pipe(postcss([autoprefixer({browsers:['last 4 version']})]))
        .pipe(cssnano({
            preset: 'advanced',
            fontFace: false,
            zindex: false
        }))
        .pipe(rename({suffix:'.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.css))
}

// Для преобразования Stylus-файлов в CSS на Production
function cssCommonProd() {
    return gulp.src(paths.app.common.styl)
        .pipe(plumber())
        .pipe(stylus({use:[nib(),rupture()]}))
        .pipe(postcss([autoprefixer({browsers:['last 4 version']})]))
        .pipe(cssnano({
            preset: 'advanced',
            fontFace: false,
            zindex: false
        }))
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest(paths.dist.css))
}

// Для объединения и минификации пользовательских JS-файлов
function jsCommon() {
    return gulp.src(paths.app.common.js)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest(paths.dist.js))
}

// Для объединения и минификации CSS-файлов внешних библиотек
function cssVendor() {
    return gulp.src(paths.app.vendor.css)
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.min.css'))
        .pipe(cssnano({discardUnused: {fontFace: false}}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.css));
}

// Для объединения и минификации CSS-файлов внешних библиотек
function cssVendorProd() {
    return gulp.src(paths.app.vendor.css)
        .pipe(concat('vendor.min.css'))
        .pipe(cssnano({discardUnused: {fontFace: false}}))
        .pipe(gulp.dest(paths.dist.css));
}

// Для объединения и минификации JS-файлов внешних библиотек
function jsVendor() {
    return gulp.src(paths.app.vendor.js)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js));
}

// Для формирования спрайта svg
function spritesSvg() {
    return gulp.src(paths.app.common.svg)
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
                $('[stroke]').removeAttr('stroke');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(svgSprite({
            mode: {
                inline: true,
                symbol: {
                    sprite: '../sprite.svg'
                }
            },
            run: function ($) {
                console.log($);
            }
        }))
        .pipe(gulp.dest(paths.dist.svg));
}

// Для обработки изображений
function img($image) {
    var $images = (typeof($image) === 'string') ? $image : paths.app.common.img;
    return gulp.src($images)
    /*.pipe(imagemin({use: [pngquant()]}))*/
        .pipe(gulp.dest(paths.dist.img));
}

// Dev task
exports.cssCommon = cssCommon;
exports.jsCommon = jsCommon;
exports.cssVendor = cssVendor;
exports.jsVendor = jsVendor;
exports.spritesSvg = spritesSvg;
exports.img = img;
exports.server = server;

gulp.task('default', gulp.series(
    gulp.parallel(cssCommon,jsCommon,cssVendor,jsVendor,fonts,spritesSvg,img,server)
));

// Prod task

exports.cssCommonProd = cssCommonProd;
exports.cssVendorProd = cssVendorProd;

gulp.task('production', gulp.series(
    gulp.parallel(cssCommonProd,jsCommon,cssVendorProd,jsVendor,fonts,spritesSvg,img,server)
));