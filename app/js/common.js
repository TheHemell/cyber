;
(function ($) {

    $(function () {

        // Fix SVG for IE
        svg4everybody();

    });

    $(document).ready(function () {

        function myTab(params) {
            var elTab = $(this).data('tab'),
                className = params.data.className,
                classNameActive = className+'_active',
                classContent = params.data.classContent;

            $('.'+className).removeClass(classNameActive);
            $(this).addClass(classNameActive);

            $('.'+classContent).removeClass(classContent+'_active');
            $('#'+elTab).addClass(classContent+'_active');
        }

        $('.b-tabs__links-item').click({
            className: "b-tabs__links-item",
            classContent: "b-tabs__contents-item"
        }, myTab);

        $('.b-stageTabs__links-item').click({
            className: "b-stageTabs__links-item",
            classContent: "b-stageTabs__contents-item"
        }, myTab);

    });

})(jQuery);